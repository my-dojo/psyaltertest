#!/bin/env/python3
import abc
import argparse
import concurrent.futures
import functools
import logging
import random
import threading
import time
import traceback
from datetime import datetime
from decimal import Decimal
from enum import Enum, auto
from random import choice
from typing import (Any, Callable, Dict, Iterable, List, Optional, Sequence,
                    Tuple, Union, cast)
from urllib.parse import urljoin

import matplotlib.pyplot as plt
import numpy as np
import requests

logging.getLogger('urllib3').setLevel(logging.ERROR)
logging.getLogger('matplotlib').setLevel(logging.ERROR)
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('client')


class RequestError(Exception):
    pass


class Account:
    id: int
    amount: Decimal
    version: int

    def __init__(self, id: int, amount: Decimal, version: int):
        self.id = id
        self.amount = amount
        self.version = version

    def __repr__(self) -> str:
        return f'Account(id={self.id}, amount="{self.amount}", version={self.version})'

    def __eq__(self, o) -> bool:
        return self.id == o.id and self.amount == o.amount and self.version == o.version

    @classmethod
    def from_json(cls, json: Dict[str, Any]) -> 'Account':
        return cls(amount=Decimal(json['amount']), id=int(json['id']), version=int(json['version']))

    @classmethod
    def from_json_many(cls, json: Sequence[Dict[str, Any]]) -> List['Account']:
        return [cls.from_json(account) for account in json]


AccountOpSuccess = Tuple[Optional[Account], ...]
AccountOpFailure = Exception
AccountOpResult = Union[AccountOpSuccess, AccountOpFailure]


class ClientStats:
    series: List[Tuple[float, float]]
    lock: threading.Lock

    def __init__(self):
        self.series = []
        self.lock = threading.Lock()

    def submit(self, start: float, end: float):
        with self.lock:
            self.series.append((start, end))

    def report(self) -> str:
        if not self.series:
            return 'No data, nothing to report!'

        durations = np.array([e - s for s, e in self.series])
        mean_duration = durations.mean()
        var_duration = durations.var()
        total_time = (
            max(e for _, e in self.series) -
            min(s for s, _ in self.series)
        )
        rps = len(self.series) / total_time
        longest = durations.max()
        shortest = durations.min()

        return f'''Total time: {total_time};å
        total requests:                        {len(self.series)};
        mean duration:                         {mean_duration};
        var:                                   {var_duration};
        rps (number of requests / total_time): {rps};
        min duration:                          {shortest};
        max duration:                          {longest}.
        '''

    def plot_durations(self, filename: str):
        durations = self._make_durations()
        plt.hist(durations, label='Durations')
        plt.savefig(fname=filename)

    def _make_durations(self) -> np.array:
        return np.array([e - s for s, e in self.series])


def timed(method):
    @functools.wraps(method)
    def wrapper(self: 'Client', *args, **kwargs):
        timer = time.time
        start = timer()
        try:
            result = method(self, *args, **kwargs)
        except self.Retry:
            self.stats.submit(start, timer())
            raise
        except Exception:
            raise
        self.stats.submit(start, timer())
        return result
    return wrapper


class Client:
    base_url: str
    retries: int
    stats: ClientStats

    class Retry(Exception):
        pass

    def __init__(self, base_url: str, retries: int = 5):
        self.base_url = base_url
        self.retries = retries
        self.reset_stats()

    @timed
    def get(self, account_id: int) -> Account:
        url = urljoin(self.base_url, f'accounts/{account_id}/')
        r = requests.get(url)
        if r.status_code != 200:
            raise RequestError(f'Got error from {url}: {r.text}')
        return Account.from_json(r.json())

    @timed
    def list(self) -> List[Account]:
        url = urljoin(self.base_url, f'accounts/')
        r = requests.get(url)
        if r.status_code != 200:
            raise RequestError(f'Got error from {url}: {r.text}')
        return Account.from_json_many(r.json())

    @timed
    def create(self, _: Optional[Account], amount: str) -> AccountOpResult:
        url = urljoin(self.base_url, 'accounts/')
        r = requests.post(url, {"amount": amount})
        if r.status_code != 201:
            raise RequestError(f'Failed to create account {url}: {r.text}')
        return None, Account.from_json(r.json())

    def deposit(self, account_id: int, amount: str) -> AccountOpResult:
        retries = self.retries
        while retries:
            url = urljoin(self.base_url, f'accounts/{account_id}/deposit/')
            try:
                return self._do_deposit(url, account_id, amount)
            except self.Retry:
                retries -= 1
                continue
        raise RequestError(f'Retries limit exceeded {url}')

    def withdraw(self, account_id: int, amount: str) -> AccountOpResult:
        retries = self.retries
        while retries:
            url = urljoin(self.base_url, f'accounts/{account_id}/withdraw/')
            try:
                return self._do_withdraw(url, account_id, amount)
            except self.Retry:
                retries -= 1
                continue
        raise RequestError(f'Retries limit exceeded {url}')

    def transfer(self, account_id: int, amount: str, account_to_id: int) -> AccountOpResult:
        retries = self.retries
        while retries:
            url = urljoin(self.base_url, f'accounts/{account_id}/transfer/')
            try:
                return self._do_transfer(url, account_id, amount, account_to_id)
            except self.Retry:
                retries -= 1
                continue
        raise RequestError(f'Retries limit exceeded {url}')

    def get_stats(self) -> ClientStats:
        return self.stats

    def reset_stats(self):
        self.stats = ClientStats()

    @timed
    def _do_deposit(self, url: str, account_id: int, amount: str) -> AccountOpResult:
        account_in = self.get(account_id)
        r = requests.post(
            url, data={'amount': amount, 'version': account_in.version})
        if r.status_code == 409:
            raise self.Retry()
        if r.status_code != 200:
            raise RequestError(f'Failed to deposit {url}: {r.text}')
        return account_in, Account.from_json(r.json())

    @timed
    def _do_withdraw(self, url: str, account_id: int, amount: str) -> AccountOpResult:
        account_in = self.get(account_id)
        r = requests.post(
            url, data={'amount': amount, 'version': account_in.version})
        if r.status_code == 409:
            raise self.Retry()
        if r.status_code == 400:
            return account_in, account_in
        if r.status_code != 200:
            raise RequestError(f'Failed to withdraw {url}: {r.text}')
        return account_in, Account.from_json(r.json())

    @timed
    def _do_transfer(self, url: str, account_id: int, amount: str, account_to_id: int) -> AccountOpResult:
        account_in = self.get(account_id)
        account_to = self.get(account_to_id)
        r = requests.post(
            url, data={
                'amount': amount,
                'version': account_in.version,
                'account_to': account_to_id,
                'version_to': account_to.version
            }
        )
        if r.status_code == 409:
            raise self.Retry()
        if r.status_code == 400:
            return account_in, account_in, account_to, account_to
        if r.status_code != 200:
            raise RequestError(f'Failed to transfer {url}: {r.text}')
        return account_in, Account.from_json(r.json()[0]), account_to, Account.from_json(r.json()[1])


class TaskStatus(int, Enum):
    Success = auto()
    WrongResult = auto()
    Fail = auto()


class TaskResult:
    status: TaskStatus
    # TODO: Should keep and account on failed status as well.
    result: AccountOpResult

    def __init__(self, result: AccountOpResult, status: TaskStatus):
        self.result = result
        self.status = status

    @classmethod
    def succeeded(cls, result: AccountOpResult) -> 'TaskResult':
        return cls(result, TaskStatus.Success)

    @classmethod
    def wrong(cls, result: AccountOpResult) -> 'TaskResult':
        return cls(result, TaskStatus.WrongResult)

    @classmethod
    def failed(cls, result: AccountOpResult) -> 'TaskResult':
        return cls(result, TaskStatus.Fail)


class Stats:
    results: List[TaskResult]
    times: List[datetime]
    lock: threading.Lock
    check_passed: bool

    def __init__(self):
        self.results = []
        self.times = []
        self.lock = threading.Lock()
        self.check_passed = False

    def submit(self, result: TaskResult) -> None:
        with self.lock:
            self.results.append(result)
            self.times.append(datetime.now())

    def get_succeeded(self) -> List[AccountOpSuccess]:
        return [cast(AccountOpSuccess, x.result) for x in self.results if x.status == TaskStatus.Success]

    def get_wrong(self) -> List[AccountOpSuccess]:
        return [cast(AccountOpSuccess, x.result) for x in self.results if x.status == TaskStatus.WrongResult]

    def get_failed(self) -> List[Exception]:
        return [cast(Exception, x.result) for x in self.results if x.status == TaskStatus.Fail]

    def get_number_of_succeeded(self) -> int:
        return len(self.get_succeeded())

    def get_number_of_wrong(self) -> int:
        return len(self.get_wrong())

    def get_number_of_failed(self) -> int:
        return len(self.get_failed())


class BaseTask(abc.ABC):
    stats: Stats
    func: Callable

    def __init__(self, func: Callable, stats: Stats):
        self.stats = stats
        # Mypy complains about it. See https://github.com/python/mypy/issues/708
        self.func = func  # type: ignore

    def __call__(self, account_id: Optional[int], data: Dict[str, Any]):
        try:
            res = self.func(account_id, **data)
        except RequestError as e:
            # logger.debug(f'Got unexpected response: {e}')
            res = e
        except Exception as e:
            # logger.debug(f'Got unexpected error: {traceback.format_exc()}')
            res = e
            # raise
        self.stats.submit(self._weak_check(res, data))

    def _weak_check(
            self,
            result: AccountOpResult,
            data: Dict[str, Any]) -> TaskResult:
        if isinstance(result, AccountOpFailure):
            return TaskResult.failed(result)
        return self._do_weak_check(result, data)

    @abc.abstractmethod
    def _do_weak_check(
            self,
            result: AccountOpSuccess,
            data: Dict[str, Any]) -> TaskResult:
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def run(cls, func: Callable, threads: int, **kwargs) -> Stats:
        raise NotImplementedError()

    @classmethod
    def do_run(cls,
               func: Callable,
               threads: int,
               tasks_args: Sequence[Tuple[Optional[int], Dict[str, Any]]]) -> Stats:
        stats = Stats()
        task = cls(func, stats)
        logger.debug(f'Got {len(tasks_args)} task(s)')
        with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as e:
            e.map(lambda x: task(*x), tasks_args)
        return stats


class CreateAccounts(BaseTask):
    def _do_weak_check(
            self,
            result: AccountOpSuccess,
            data: Dict[str, Any]) -> TaskResult:
        amount = Decimal(data['amount'])
        _, account_out = result
        if account_out and account_out.version == 0 and account_out.amount == amount and amount >= 0:
            return TaskResult.succeeded(result)
        return TaskResult.wrong(result)

    @classmethod
    def run(cls, func: Callable, threads: int, **kwargs) -> Stats:
        count: int = kwargs['count']
        logger.info(f'Creating {count} accounts in {threads} threads')
        data = [(None, {'amount': _amount_gauss_abs(5000, 1000)})
                for i in range(count)]
        return cls.do_run(func, threads, data)


class Deposit(BaseTask):
    def _do_weak_check(
            self,
            result: AccountOpSuccess,
            data: Dict[str, Any]) -> TaskResult:
        amount = Decimal(data['amount'])
        account_in, account_out = result
        if (
            account_in and account_out and
            account_out.amount == account_in.amount + amount and
            account_out.version == account_in.version + 1
        ):
            return TaskResult.succeeded(result)
        return TaskResult.wrong(result)

    @classmethod
    def run(cls, func: Callable, threads: int, **kwargs) -> Stats:
        accounts: Sequence[int] = kwargs['accounts']
        logger.info(
            f'Depositing {len(accounts)} accounts in {threads} threads')
        data = [(account, {'amount': _amount_gauss_abs(5000, 1000)})
                for account in accounts] * 4
        random.shuffle(data)
        return cls.do_run(func, threads, data)


class Withdraw(BaseTask):
    def _do_weak_check(
            self,
            result: AccountOpSuccess,
            data: Dict[str, Any]) -> TaskResult:
        account_in, account_out = result
        amount = Decimal(data['amount'])
        if (
            account_in and account_out and
            account_out.amount == account_in.amount - amount and
            account_out.version == account_in.version + 1
        ):
            return TaskResult.succeeded(result)
        if (
            account_in and account_out and
            account_out == account_in and
            account_in.amount < amount
        ):
            return TaskResult.succeeded(result)
        return TaskResult.wrong(result)

    @classmethod
    def run(cls, func: Callable, threads: int, **kwargs) -> Stats:
        accounts: Sequence[int] = kwargs['accounts']
        logger.info(
            f'Withdraw {len(accounts)} accounts in {threads} threads')
        data = [(account, {'amount': _amount_gauss_abs(5000, 3000)})
                for account in accounts] * 4
        random.shuffle(data)
        return cls.do_run(func, threads, data)


class Transfer(BaseTask):
    def _do_weak_check(
            self,
            result: AccountOpSuccess,
            data: Dict[str, Any]) -> TaskResult:
        account_from_in, account_from_out, account_to_in, account_to_out = result
        amount = Decimal(data['amount'])
        if (
            account_from_in and account_from_out and account_to_in and account_to_out and
            account_from_out.amount == account_from_in.amount - amount and
            account_from_out.version == account_from_in.version + 1 and
            account_to_out.amount == account_to_in.amount + amount and
            account_to_out.version == account_to_in.version + 1
        ):
            return TaskResult.succeeded(result)
        if (
            account_from_in and account_from_out and
            account_from_out == account_from_in and
            account_from_in.amount < amount
        ):
            return TaskResult.succeeded(result)
        return TaskResult.wrong(result)

    @classmethod
    def run(cls, func: Callable, threads: int, **kwargs) -> Stats:
        accounts: Sequence[int] = kwargs['accounts']
        logger.info(
            f'Transfer {len(accounts)} accounts in {threads} threads')
        data: List[Tuple[int, Dict]] = [(account, {'amount': _amount_gauss_abs(2000, 300)})
                                        for account in accounts] * 4
        ids = frozenset(a for a in accounts)
        for d in data:
            d[1]['account_to_id'] = choice(list(ids - {d[0]}))
        random.shuffle(data)
        return cls.do_run(func, threads, data)


def _amount_gauss_abs(median: float, sigma: float) -> str:
    return f'{abs(random.gauss(5000, 1000)):f}'


def list_accounts_command(base_url, **kwargs):
    client = Client(base_url)
    list_accounts(client)


def log_report(stage: str, stats: Stats, client_stats: ClientStats, logger: logging.Logger):
    failed = stats.get_failed()
    wrong = stats.get_wrong()
    total = len(stats.results)
    logger.info(
        f'{stats.get_number_of_succeeded()} out of {total} accounts handeled'
    )
    logger.info(
        f'Wrong: {len(wrong)}, failed: {len(failed)}'
    )
    logger.info(client_stats.report())
    client_stats.plot_durations(f'{stage}.png')

    for e in stats.get_failed():
        assert isinstance(e, Exception), str(type(e))
        logger.error(f'Error occurred during execution of {stage}: {e}')


def run_tests_command(count: int, threads: int, base_url: str, **kwargs):
    client = Client(base_url)

    stats = CreateAccounts.run(client.create, threads, count=count)
    accounts = {cast(Account, a).id for _, a in stats.get_succeeded()}
    log_report('creation', stats, client.stats, logger)

    client.reset_stats()
    stats = Deposit.run(client.deposit, threads, accounts=accounts)
    accounts = {cast(Account, a).id for a, _ in stats.get_succeeded()}
    log_report('depositing', stats, client.stats, logger)

    client.reset_stats()
    stats = Withdraw.run(client.withdraw, threads, accounts=accounts)
    accounts = {cast(Account, a).id for a, _ in stats.get_succeeded()}
    log_report('withdrawing', stats, client.stats, logger)

    client.reset_stats()
    stats = Transfer.run(client.transfer, threads, accounts=accounts)
    log_report('transferring', stats, client.stats, logger)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--threads', type=int, default=1)
    parser.add_argument('--base_url', type=str,
                        default='http://localhost:8000')
    parser.add_argument('--retries', type=int, default=5)
    sub = parser.add_subparsers()

    test = sub.add_parser('test')
    test.add_argument('--count', type=int, default=1000)
    test.set_defaults(command=run_tests_command)

    args = parser.parse_args()
    args.command(**vars(args))
