provider "google" {
  credentials = file("../../secrets/terraform-gcp-key.json")
  project     = "mgserjio"
  region      = "us-central1"
}

resource "google_compute_address" "static" {
  name = "psyalter-ip"
}

resource "google_compute_instance" "default" {
  name         = "psyalter-vm"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = "30"
      type  = "pd-standard"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }
}
