#!/bin/sh
set -e

echo "Migrating ..."
python manage.py migrate

echo "Running command ..."
exec "$@"
