from datetime import datetime
from enum import Enum

from django.db import models, transaction

from accounts.utils import decimal_field_factory

DecimalField = decimal_field_factory(models.DecimalField)


class Account(models.Model):

    class AccountVersionChangedError(Exception):
        pass

    class InsufficientFundsError(Exception):
        pass

    class AccountIsClosedError(Exception):
        pass

    # user = models.ForeignKey()
    actual = models.OneToOneField(
        'AccountRecord', on_delete=models.PROTECT, related_name='account_ref', null=True)
    version = models.IntegerField(null=False)
    closed = models.BooleanField(null=False, default=False)

    def deposit(self, amount):
        self._update_balance(amount + self.actual.amount)

    def withdraw(self, amount):
        if self.actual.amount < amount:
            raise self.InsufficientFundsError('Insufficient funds')
        self._update_balance(self.actual.amount - amount)

    def _update_balance(self, balance):
        if self.closed:
            raise self.AccountIsClosedError()

        with transaction.atomic():
            # TODO: Move record operations to records.

            account_record = self.actual.create_successor(
                amount=balance, timestamp=datetime.now()
            )

            updated = Account.objects.filter(
                id=self.id, version=self.version
            ).update(actual=account_record, version=self.version + 1)
            if updated != 1:
                raise self.AccountVersionChangedError()

        # To avoid requesting db again, update object inplace.
        self.version = self.version + 1
        self.actual = account_record


class AccountRecord(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    amount = DecimalField()
    parent = models.ForeignKey(
        'AccountRecord', on_delete=models.PROTECT, null=True)
    timestamp = models.DateTimeField()

    class Meta:
        indexes = [
            models.Index(fields=['account']),
        ]

    def create_successor(self, amount, timestamp):
        return AccountRecord.objects.create(
            account=self.account,
            parent=self,
            amount=amount,
            timestamp=timestamp,
        )


# TODO: Move to own file.
class TransactionStatuses(Enum):
    Committed = 'CO'
    InsufficientAmount = 'IA'
    Created = 'CR'

    @classmethod
    def choices(cls):
        return [[x.value, x.name] for x in cls]


class TransactionLog(models.Model):
    account_src = models.ForeignKey(
        Account, on_delete=models.PROTECT, related_name='account_src')
    account_dst = models.ForeignKey(
        Account, on_delete=models.PROTECT, null=True, related_name='account_dst')
    account_record_src = models.ForeignKey(
        AccountRecord, on_delete=models.PROTECT, related_name='account_record_src')
    account_record_dst = models.ForeignKey(
        AccountRecord, on_delete=models.PROTECT, null=True, related_name='account_record_dst')
    amount = DecimalField()
    status = models.CharField(
        max_length=4, choices=TransactionStatuses.choices())
