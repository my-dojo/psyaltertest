from datetime import datetime

from rest_framework import serializers
from rest_framework.fields import IntegerField

from accounts.models import Account, AccountRecord
from accounts.utils import decimal_field_factory

DecimalField = decimal_field_factory(serializers.DecimalField)


class AccountDetailSerializer(serializers.ModelSerializer):
    amount = serializers.SerializerMethodField()

    class Meta:
        model = Account
        fields = ['id', 'amount', 'closed', 'version']

    def get_amount(self, account):
        return str(account.actual.amount)


class AccountOperationBaseSerializer(serializers.Serializer):
    amount = DecimalField()

    def validate(self, data):
        if data['amount'] < 0:
            raise serializers.ValidationError("Amount cannot be negative.")
        return data


class AccountBalanceOperationsSerializer(AccountOperationBaseSerializer):
    version = IntegerField()


class AccountTransferSerializer(AccountBalanceOperationsSerializer):
    account_to = serializers.IntegerField()
    version_to = IntegerField()


class AccountOpeningSerializer(AccountOperationBaseSerializer):
    def create(self, validated_data):
        account = Account.objects.create(version=0)
        record = account.accountrecord_set.create(
            amount=validated_data['amount'], timestamp=datetime.now())
        account.actual = record
        account.save()
        return account
