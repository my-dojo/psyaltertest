from datetime import datetime

from django.test import TestCase

from accounts.models import Account


class AccountDetailsTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.account1 = Account.objects.create(version=0)
        cls.account2 = Account.objects.create(version=0)

        cls.account1.actual = cls.account1.accountrecord_set.create(
            amount='1000.000000', timestamp=datetime.now())
        cls.account1.save()

        cls.account2.actual = cls.account2.accountrecord_set.create(
            amount='2000.000000', timestamp=datetime.now()
        )
        cls.account2.save()

    def test_list(self):
        accounts = self.client.get('/accounts/').json()
        self.assertGreaterEqual(len(accounts), 2,
                                'There should be two accounts in db.')
        self.assertIn({'id': 1, 'amount': '1000.000000',
                       'closed': False, 'version': 0}, accounts)
        self.assertIn({'id': 2, 'amount': '2000.000000',
                       'closed': False, 'version': 0}, accounts)

    def test_retrieve(self):
        account1 = self.client.get('/accounts/1/').json()
        self.assertEqual(
            account1, {'id': 1, 'amount': '1000.000000', 'closed': False, 'version': 0})

    def test_create(self):
        r = self.client.post('/accounts/', {'amount': '3000.000000'})
        account_id = r.json()['id']
        self.assertEqual(r.status_code, 201)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '3000.000000', 'closed': False, 'version': 0})

        r = self.client.get(f'/accounts/{account_id}/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '3000.000000', 'closed': False, 'version': 0})

    def test_close(self):
        r = self.client.post('/accounts/', {'amount': '4000.000000'})
        self.assertFalse(r.json()['closed'])
        account_id = r.json()['id']
        r = self.client.post(f'/accounts/{account_id}/close/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '4000.000000', 'closed': True, 'version': 0})

        r = self.client.get(f'/accounts/{account_id}/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '4000.000000', 'closed': True, 'version': 0})

    def test_deposit(self):
        r = self.client.post('/accounts/', {'amount': '4000.000000'})
        account_id = r.json()['id']

        r = self.client.post(
            f'/accounts/{account_id}/deposit/', {'amount': '1500', 'version': 0})
        self.assertEqual(r.status_code, 200, r.content)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '5500.000000', 'closed': False, 'version': 1})

    def test_withdraw(self):
        r = self.client.post('/accounts/', {'amount': '4000.000000'})
        account_id = r.json()['id']

        r = self.client.post(
            f'/accounts/{account_id}/withdraw/', {'amount': '1500', 'version': 0})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '2500.000000', 'closed': False, 'version': 1})

        r = self.client.post(
            f'/accounts/{account_id}/withdraw/', {'amount': '2500', 'version': 1})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(), {'id': account_id, 'amount': '0.000000', 'closed': False, 'version': 2})

        r = self.client.post(
            f'/accounts/{account_id}/withdraw/', {'amount': '2500', 'version': 2})
        self.assertEqual(r.status_code, 400)

    def test_transfer(self):
        r = self.client.post('/accounts/', {'amount': '4000.000000'})
        account_from_id = r.json()['id']
        r = self.client.post('/accounts/', {'amount': '2000.000000'})
        account_to_id = r.json()['id']

        r = self.client.post(
            f'/accounts/{account_from_id}/transfer/',
            {'account_to': account_to_id, 'amount': '2500', 'version': 0}
        )
        self.assertEqual(r.status_code, 200)
        self.assertEqual(
            r.json(),
            [
                {'id': account_from_id, 'amount': '1500.000000',
                    'closed': False, 'version': 1},
                {'id': account_to_id, 'amount': '4500.000000',
                    'closed': False, 'version': 1}
            ]
        )
