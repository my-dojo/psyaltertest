from django.urls import path
from rest_framework import routers

from accounts.views import AccountDetail

router = routers.SimpleRouter()
router.register('', AccountDetail, basename='accounts')
urlpatterns = router.urls
