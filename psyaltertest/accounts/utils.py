def decimal_field_factory(cls, max_digits=30, decimal_places=6, *args, **kwargs):
    class DecimalField(cls):
        def __init__(self, max_digits=30, decimal_places=6, *args, **kwargs):
            super().__init__(max_digits=max_digits,
                             decimal_places=decimal_places,  *args, **kwargs)
    return DecimalField
