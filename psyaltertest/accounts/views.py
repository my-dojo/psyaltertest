from decimal import Decimal

from django.db import transaction
from django.shortcuts import render
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from accounts.models import Account, AccountRecord, TransactionLog
from accounts.serializers import (AccountBalanceOperationsSerializer,
                                  AccountDetailSerializer,
                                  AccountOpeningSerializer,
                                  AccountTransferSerializer)


class InsufficientFundsError(APIException):
    status_code = 400
    default_detail = 'Cannot withdraw due to insufficient funds.'
    default_code = 'insufficient_funds'


class AccountIsClosedError(APIException):
    status_code = 400
    default_detail = 'Operations on closed accont are not allowed.'
    default_code = 'accont_is_closed'


class AccountVersionMismatch(APIException):
    status_code = 409
    default_detail = 'Account version mismatch, please update account and retry.'
    default_code = 'account_version_mismatch'


class OutOfRetriesError(APIException):
    status_code = 500
    default_detail = 'Retries limit exceeded, please try again in a moment.'
    default_code = 'retries_limit_exceeded'

# TODO: Add transactions logging to db.


class AccountDetail(ViewSet):
    # TODO: It would be better to pass version though API and force client to do
    #       retries if modification failed.

    def list(self, request):
        s = AccountDetailSerializer(self._queryset, many=True)
        return Response(s.data)

    # TODO: Why "pk=None"? Check without default None.
    def retrieve(self, request, pk=None):
        return self._account_detail(self._queryset.get(id=pk))

    def create(self, request):
        s = AccountOpeningSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        account = s.create(request.data)
        return self._account_detail(account, status=201)

    @action(methods=['post'], detail=True)
    def close(self, request, pk=None):
        account = self._queryset.get(id=pk)
        account.closed = True
        account.save()
        return self._account_detail(account)

    @action(methods=['post'], detail=True)
    def deposit(self, request, pk=None):
        s = AccountBalanceOperationsSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        account = self._get_and_check_version(pk, s.validated_data['version'])
        amount = s.validated_data['amount']
        self._update_balance(account, Account.deposit, amount)
        return self._account_detail(account)

    @action(methods=['post'], detail=True)
    def withdraw(self, request, pk=None):
        s = AccountBalanceOperationsSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        account = self._get_and_check_version(pk, s.validated_data['version'])
        amount = s.validated_data['amount']
        self._update_balance(account, Account.withdraw, amount)
        return self._account_detail(account)

    @action(methods=['post'], detail=True)
    def transfer(self, request, pk=None):
        s = AccountTransferSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        amount = s.validated_data['amount']
        with transaction.atomic():
            account_from = self._get_and_check_version(
                pk, s.validated_data['version'])
            account_to = self._get_and_check_version(
                s.validated_data['account_to'], version=s.validated_data['version_to'])
            self._update_balance(account_from, Account.withdraw, amount)
            self._update_balance(account_to, Account.deposit, amount)
        s = AccountDetailSerializer([account_from, account_to], many=True)
        return Response(s.data)

    def _get_and_check_version(self, pk, version):
        account = self._queryset.get(id=pk)
        if account.version != version:
            raise AccountVersionMismatch()
        return account

    def _update_balance(self, account, func, amount):
        try:
            func(account, amount)
        except Account.AccountVersionChangedError:
            raise AccountVersionMismatch()
        except Account.InsufficientFundsError:
            raise InsufficientFundsError()
        except Account.AccountIsClosedError:
            raise AccountIsClosedError()

    def _account_detail(self, account, *args, **kwargs):
        s = AccountDetailSerializer(account)
        return Response(s.data, *args, **kwargs)

    @property
    def _queryset(self):
        return Account.objects.all()


#   class Transactions(APIView):
#       ...
