#!/usr/bin/env python3

import subprocess
import sys
from itertools import chain

import fire
from plumbum import local


class DockerCompose():
    project_name: str
    dry_run: bool

    def __init__(self, project_name='psyaltertest_stage', dry_run=False):
        self.project_name = project_name
        self.dry_run = dry_run

        compose_args = [
            '-p', self.project_name,
            '-f', 'docker-compose.yml',
            '-f', 'docker-compose.stage.yml',
        ]
        self._compose_cmd = local['docker-compose'][compose_args]

    def build(self, *args, **kwargs):
        self._compose('build', *args, **kwargs)

    def up(self, *args, **kwargs):
        self._compose('up', '-d', *args, **kwargs)

    def down(self, **kwargs):
        self._compose('down', **kwargs)

    def start(self, *args, **kwargs):
        self._compose('stop', *args, **kwargs)

    def stop(self, *args, **kwargs):
        self._compose('stop', *args, **kwargs)

    def run(self, service, *args, **kwargs):
        self._compose('start', service, *args, **kwargs)

    def restart(self, service, *args, **kwargs):
        self._compose('restart', service, *args, **kwargs)

    def logs(self, service, *args, **kwargs):
        self._compose('logs', service, *args, **kwargs)

    def sh(self, service):
        self._compose('exec', service, 'sh')

    def _compose(self, *args, **kwargs):
        # TODO: This still does non work as expected:
        # Example:
        # > ./routiner.py --dry_run True up --build
        # Expected:
        #   /usr/local/bin/docker-compose -p psyaltertest_stage -f docker-compose.yml -f docker-compose.stage.yml up -d --build
        # Actual:
        #   /usr/local/bin/docker-compose -p psyaltertest_stage -f docker-compose.yml -f docker-compose.stage.yml up -d build True
        #                                                                                                               ^     ^
        cmd = self._compose_cmd[[*args, list(chain.from_iterable(kwargs.items()))]]
        if self.dry_run:
            print(f'Going to run:\n  {cmd}')
            return
        print(f"Running `{cmd}' ...")
        p = cmd.popen(bufsize=0, stdin=0, stdout=1, stderr=subprocess.STDOUT)
        status_code = p.wait()
        if status_code:
            exit(status_code)


if __name__ == '__main__':
    fire.Fire(DockerCompose)
